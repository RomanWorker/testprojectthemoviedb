﻿import { createAction, handleActions } from 'redux-actions'

export const setCreditsForMovies = createAction('SET_CREDITS_FOR_MOVIE', creditsMovie => ({ creditsMovie }));

export const creditsMoviesReducer = handleActions({
    [setCreditsForMovies]: (state, action) => addNewCredits(action.payload.creditsMovie, state)
}, []);

function addNewCredits(creditsMovie, credits) {
    let tmpIndex = credits.findIndex(credit => credit.id === creditsMovie.id);

    if (tmpIndex > -1) {
        credits[tmpIndex] = creditsMovie;
    } else {
        credits.push(creditsMovie);
    }

    return Array.from(credits);
}
