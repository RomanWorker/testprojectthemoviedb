﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TestProject.API.Models
{
    public class UserInfoModel
    {
        [JsonProperty(PropertyName = "user_id")]
        public string UserId { get; set; }

        [JsonProperty(PropertyName = "first_name")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "last_name")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "user_name")]
        public string UserName { get; set; }

        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "movies_for_watch")]
        public IEnumerable<int> MoviesForWatch { get; set; }
    }
}
