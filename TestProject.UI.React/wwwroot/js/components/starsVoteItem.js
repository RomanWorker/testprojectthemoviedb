﻿import React from 'react';

import { WebConfig } from '../webConfig';

export default class StarsVoteItem extends React.Component {
    static get goodStar() {
        return '<img src="' + WebConfig.IMAGE_PATH + 'star1.png" />';
    }
    static get badStar() {
        return '<img src="' + WebConfig.IMAGE_PATH + 'star2.png" />';
    }
    static get minVote() {
        return 20;
    }

    constructor(props) {
        super(props);
    }

    getStars(countStars) {
        let tmpStars = '';

        for (var i = 1; i <= 10; i++) {
            tmpStars += i < countStars ? StarsVoteItem.goodStar : StarsVoteItem.badStar;
        }

        return <div dangerouslySetInnerHTML={{ __html: tmpStars }} />;
    }

    makeRating(params) {
        if (params.voteCount > StarsVoteItem.minVote)
            return this.getStars(params.voteAverage);

        return params.failMessage;
    }
    
    render() {
        const result = this.makeRating({
            voteAverage: this.props.voteAverage,
            voteCount: this.props.voteCount,
            failMessage: this.props.content.ratingHasNotDone
        });

        return (
            <div>
                {result}
            </div>
        );
    }
}
