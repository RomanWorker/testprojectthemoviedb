import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.url.endsWith('.json') || req.url.startsWith('http')) {
            return next.handle(req);
        }
        const newRequest = req.clone({ url: `${environment.apiUrl}${req.url}` });
        return next.handle(newRequest);
    }
}
