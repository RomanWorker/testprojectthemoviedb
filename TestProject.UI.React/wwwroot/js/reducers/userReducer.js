﻿import { createAction, handleActions } from 'redux-actions'

const initUser = {
    userId: 1,
    login: 'guest',
    isAuth: false,
    password: '',
    error: ''
}

const initTestUser = {
    userId: 2,
    login: 'user',
    isAuth: true,
    password: '',
    error: ''
}

export const setUser = createAction('SET_CURRENT_USER', user => ({ user }));

export const userReducer = handleActions({
    [setUser]: (state, action) => action.payload.user
}, initUser);
