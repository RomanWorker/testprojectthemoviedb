export class MovieDetails {
    id: number;
    budget: number;
    revenue: number;
    overview: string;
    runtime: number;
}
