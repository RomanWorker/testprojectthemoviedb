﻿import { createAction, handleActions } from 'redux-actions'

export const setWishMovies = createAction('SET_WISH_MOVIES', wishMovies => ({ wishMovies }));
export const addNewWishMovies = createAction('ADD_NEW_WISH_MOVIE', newWishMovie => ({ newWishMovie }));
export const deleteWishMovies = createAction('DELETE_WISH_MOVIE_REDUCER', delWishMovie => ({ delWishMovie }));

export const wishMoviesReducer = handleActions({
    [setWishMovies]: (state, action) => action.payload.wishMovies,
    [addNewWishMovies]: (state, action) => addNewWishMovie(state, action.payload.newWishMovie),
    [deleteWishMovies]: (state, action) => deleteWishMovie(state, action.payload.delWishMovie)
}, []);

function addNewWishMovie(wishMovies, newWishMovie) {
    wishMovies.push(newWishMovie);
    return Array.from(wishMovies);
}

function deleteWishMovie(wishMovies, delWishMovie) {
    let delIndex = wishMovies.indexOf(delWishMovie);
    wishMovies.splice(delIndex, 1);
    return Array.from(wishMovies);
}
