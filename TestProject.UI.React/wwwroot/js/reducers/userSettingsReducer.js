﻿import { createAction, handleActions } from 'redux-actions'

const initState = {
    categories: [],
    isSuccess: null
}

export const setUserSettings = createAction('SET_USER_SETTINGS', settings => ({ settings }));

export const userSettingsReducer = handleActions({
    [setUserSettings]: (state, action) => action.payload.settings
}, initState);
