﻿import { createAction, handleActions } from 'redux-actions'

export const setSelectedCategoryForMovies = createAction('SET_SELECTED_CATEGORY_FOR_MOVIES', category => ({ category }));

export const selectedCategoryReducer = handleActions({
    [setSelectedCategoryForMovies]: (state, action) => action.payload.category
}, null);
