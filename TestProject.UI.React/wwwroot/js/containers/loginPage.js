﻿import React from 'react';
import { connect } from 'react-redux';

import LoginForm from '../components/loginForm';

import { tryAuthUser } from '../actions/authActions';

class LoginPage extends React.Component {
    render() {
        return (
            <div className="row">
                <div className="col-md-4 col-md-offset-4">
                    <LoginForm
                        user={this.props.user}
                        content={this.props.content}
                        loginAction={this.props.loginAction} />
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.user,
        content: state.content
    };
}

function mapActionsToProps(dispatch) {
    return {
        loginAction(user) {
            dispatch(tryAuthUser(user));
        }
    }
}

export default connect(mapStateToProps, mapActionsToProps)(LoginPage);