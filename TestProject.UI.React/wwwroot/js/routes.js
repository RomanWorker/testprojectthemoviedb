﻿import React from 'react';
import { Route } from 'react-router-dom';

import Layout from './components/layout'
import LoginPage from './containers/loginPage'
import MoviesPage from './containers/moviesPage'
import SettingsPage from './containers/settingsPage'

export default class extends React.Component {
    render() {
        return (
            <Layout>
                <Route exact path="/" component={MoviesPage} />
                <Route path="/movies" component={MoviesPage} />
                <Route path="/login" component={LoginPage} />
                <Route path="/settings" component={SettingsPage} />
            </Layout>
        );
    }
}
