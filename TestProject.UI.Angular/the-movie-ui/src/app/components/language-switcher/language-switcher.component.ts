import { Component } from '@angular/core';

import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-language-switcher',
  templateUrl: './language-switcher.component.html',
  styleUrls: ['./language-switcher.component.css']
})
export class LanguageSwitcherComponent {
  constructor(private userService: UserService) { }

  switchLanguage(language: string) {
    this.userService.changeLanguage(language);
  }
}
