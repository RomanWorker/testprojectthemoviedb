﻿import React from 'react';
import NavigationBar from '../containers/navigationBar';

export default class Layout extends React.Component {
    render() {
        return (
            <div className="container">
                <NavigationBar />
                {this.props.children}
            </div>
        );
    }
}