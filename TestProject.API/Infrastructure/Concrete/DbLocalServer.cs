﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TestProject.API.Infrastructure.Entities;

namespace TestProject.API.Infrastructure.Concrete
{
    public class DbLocalServer
    {
        private readonly string _connectionString;

        private IDbConnection GetDbConnection()
        {
            return new SqlConnection(_connectionString);
        }

        public DbLocalServer(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IEnumerable<UserCategory> GetUserCategories(int userId)
        {
            using (var conn = GetDbConnection())
            {
                return conn.Query<UserCategory>(
                    @"
                      SELECT
                        utc.UserId,
                        utc.CategoryId,
                        utc.OrderNumber,
                        utc.IsShow,
                        ctg.CodeName,
                        ctg.GenreId
                      FROM dbo.UserToCategory utc
                        INNER JOIN dbo.Category ctg ON ctg.CategoryId = utc.CategoryId
                      WHERE utc.UserId = @userId
                     ",
                    new { userId });
            }
        }

        public IEnumerable<UserCategory> GetUserSettingCategories(int userId)
        {
            using (var conn = GetDbConnection())
            {
                return conn.Query<UserCategory>(
                    @"
                      SELECT 
                        ctg.CodeName,
                        ctg.GenreId,
                        ctg.CategoryId,
                        utc.UserId,
                        utc.OrderNumber,
                        utc.IsShow                     
                      FROM dbo.Category ctg
                        LEFT JOIN dbo.UserToCategory utc ON utc.CategoryId = ctg.CategoryId
                                                        AND utc.UserId     = @userId
                     ",
                    new { userId });
            }
        }

        public IEnumerable<User> GetUser(int? userId = null, string login = null, string passHash = null)
        {
            using (var conn = GetDbConnection())
            {
                return conn.Query<User>(
                    @"
                      SELECT
                        UserId,
                        Login,
                        PasswordHash
                      FROM dbo.[User]
                      WHERE (@userId   IS NULL OR @userId   = UserId)
                        AND (@login    IS NULL OR @login    = Login)
                        AND (@passHash IS NULL OR @passHash = PasswordHash)
                     ",
                    new { userId, login, passHash });
            }
        }

        public IEnumerable<WishMovie> GetWishMovies(int userId)
        {
            using (var conn = GetDbConnection())
            {
                return conn.Query<WishMovie>(
                    @"
                      SELECT
                        UserId,
                        MovieId
                      FROM dbo.WishMovie
                      WHERE @userId IS NULL OR @userId = UserId
                     ",
                    new { userId });
            }
        }

        public int SetWishMovie(WishMovie wishMovie)
        {
            using (var conn = GetDbConnection())
            {
                return conn.Execute(
                    @"
                      IF NOT EXISTS(
                        SELECT 1 
                        FROM dbo.WishMovie
                        WHERE UserId  = @userId
                          AND MovieId = @movieId)
                      BEGIN
                        INSERT INTO dbo.WishMovie(UserId, MovieId)
                        VALUES (@userId, @movieId)
                      END
                     ",
                    new
                    {
                        userId = wishMovie.UserId,
                        movieId = wishMovie.MovieId
                    });
            }
        }

        public int DeleteWishMovie(WishMovie wishMovie)
        {
            using (var conn = GetDbConnection())
            {
                return conn.Execute(
                    @"
                      DELETE FROM dbo.WishMovie
                      WHERE UserId  = @userId
                        AND MovieId = @movieId
                     ",
                    new
                    {
                        userId = wishMovie.UserId,
                        movieId = wishMovie.MovieId
                    });
            }
        }

        public int UpdateUserToCategory(List<UserCategory> userCategories)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var transaction = conn.BeginTransaction())
                {
                    try
                    {
                        var result = DoUpdateUserToCategory(userCategories, conn, transaction);

                        transaction.Commit();
                        return result;
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        private int DoUpdateUserToCategory(List<UserCategory> userCategories, IDbConnection connection, IDbTransaction transaction)
        {
            DeleteUserToCategoryById(userCategories.First().UserId.Value, connection, transaction);

            var result = 0;
            foreach (var category in userCategories)
            {
                if (!category.UserId.HasValue || !category.OrderNumber.HasValue || !category.IsShow.HasValue)
                    continue;

                result += InsertUserToCategory(category, connection, transaction);
            }

            return result;
        }

        private int DeleteUserToCategoryById(int userId, IDbConnection connection, IDbTransaction transaction)
        {
            if (connection.State == ConnectionState.Closed)
                connection.Open();

            return connection.Execute(@"
                        DELETE FROM dbo.UserToCategory
                        WHERE UserId = @userId
                     ", new { userId }, transaction);
        }

        private int InsertUserToCategory(UserCategory userCategory, IDbConnection connection, IDbTransaction transaction)
        {
            if (connection.State == ConnectionState.Closed)
                connection.Open();

            return connection.Execute(@"
                          INSERT INTO dbo.UserToCategory(UserId, CategoryId, OrderNumber, IsShow)
                          VALUES (@userId, @categoryId, @orderNumber, @isShow)
                     ", new
                     {
                        userId = userCategory.UserId,
                        categoryId = userCategory.CategoryId,
                        orderNumber = userCategory.OrderNumber,
                        isShow = userCategory.IsShow
                     }, transaction);
        }
    }
}
