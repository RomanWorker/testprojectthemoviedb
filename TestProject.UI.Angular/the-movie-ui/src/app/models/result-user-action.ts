import { User } from './user';

export class ResultUserAction {
    isSuccess: boolean;
    user: User;
    errorMessage: string;
}
