export class ApiMovieModule {
    id: number;
    name: string;
    title: string;
    overview: string;
    poster_path: string;
    vote_average: number;
    release_date: Date;
}
