﻿using DM.MovieApi;
using DM.MovieApi.ApiResponse;
using DM.MovieApi.MovieDb.Genres;
using DM.MovieApi.MovieDb.Movies;
using System.Collections.Generic;

namespace TestProject.API.Infrastructure.Concrete
{
    public class DbRemoteServer
    {
        private IApiMovieRequest _movieApi;
        private IApiGenreRequest _genreApi;

        private IApiMovieRequest MovieApi
        {
            get
            {
                if (_movieApi == null)
                    _movieApi = MovieDbFactory.Create<IApiMovieRequest>().Value;
                return _movieApi;
            }
        }

        public IApiGenreRequest GenreApi
        {
            get
            {
                if (_genreApi == null)
                    _genreApi = MovieDbFactory.Create<IApiGenreRequest>().Value;
                return _genreApi;
            }
        }

        public DbRemoteServer(string apiKey, string apiUrl)
        {
            MovieDbFactory.RegisterSettings(apiKey, apiUrl);
        }

        public ApiSearchResponse<Movie> GetNowPlaying(int pageNumber = 1, string language = "en")
        {
            return MovieApi.GetNowPlayingAsync(pageNumber, language).Result;
        }

        public ApiQueryResponse<IReadOnlyList<Genre>> GetGenreForMovies(string language = "en")
        {
            return GenreApi.GetMoviesAsync(language).Result;
        }

        public ApiSearchResponse<MovieInfo> GetMoviesByGenreId(int genreId, int pageNumber = 1, string language = "en")
        {
            return GenreApi.FindMoviesByIdAsync(genreId, pageNumber, language).Result;
        }

        public ApiQueryResponse<Movie> GetMovieByMovieId(int movieId, string language = "en")
        {
            return MovieApi.FindByIdAsync(movieId, language).Result;
        }

        public ApiQueryResponse<MovieCredit> GetCreditsByMovieId(int movieId, string language = "en")
        {
            return MovieApi.GetCreditsAsync(movieId, language).Result;
        }
    }
}
