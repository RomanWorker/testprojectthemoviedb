﻿import React from 'react';
import { connect } from 'react-redux';

import CreditsForMovieItem from './creditsForMoviesItem';
import { addWishMovie, deleteWishMovie } from '../actions/wishMoviesActions';

import { WebConfig } from '../webConfig';

class DetailsForMovieItem extends React.Component {
    constructor(props) {
        super(props);

        this.addNewWishMovie = this.addNewWishMovie.bind(this);
        this.deleteWishMovie = this.deleteWishMovie.bind(this);
    }

    addNewWishMovie(userId, movieId) {
        this.props.addWishMovie(userId, movieId);
    }

    deleteWishMovie(userId, movieId) {
        this.props.delWishMovie(userId, movieId);
    }

    getBtnWishMovie(userId, movieId, wishMovies, content) {
        let wishMovie = wishMovies.find(wishMid => wishMid === movieId);

        if (wishMovie === undefined) {
            return (
                <button
                    className="btn btn-info"
                    onClick={() => this.addNewWishMovie(userId, movieId)}>
                    {content.btnWantToWatch}
                </button>
            );
        } else {
            return (
                <button
                    className="btn btn-info"
                    onClick={() => this.deleteWishMovie(userId, movieId)}>
                    {content.btnNotWantToWatch}
                </button>
            );
        }
    }

    render() {
        const movieId = this.props.movieId;
        const details = this.props.detailsMovies.find(dtls => dtls.id === movieId);

        if (details == undefined || details.isShowPage === false)
            return null;
       
        const content = this.props.content;
        const btnWishMovie = this.getBtnWishMovie(this.props.user.userId, movieId, this.props.wishMovies, content);

        return (
            <div>
                <div className="col-md-7 padding-left-0px">
                    <div className="col-md-3 padding-left-0px margin-right-15px">{content.budget}: </div>
                    <div className="col-md-8 padding-left-0px">${details.budget}</div>
                    <div className="col-md-3 padding-left-0px margin-right-15px">{content.revenue}: </div>
                    <div className="col-md-8 padding-left-0px">${details.revenue}</div>
                </div>
                <div className="col-md-4 padding-left-0px">
                    {btnWishMovie}
                </div>
                <div className="col-md-12 padding-left-0px">
                    <div className="col-md-2 padding-left-right-0px">{content.overview}: </div>
                    <div className="col-md-10 padding-left-0px">{details.overview}</div>
                    <div className="col-md-2 padding-left-right-0px">{content.runtime}: </div>
                    <div className="col-md-10 padding-left-0px">{details.runtime}</div>
                    <div className="col-md-12 padding-left-0px"> 
                        <CreditsForMovieItem movieId={movieId} />
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.user,
        detailsMovies: state.detailsMovies,
        wishMovies: state.wishMovies,
        content: state.content
    };
}

function mapActionsToProps(dispatch) {
    return {
        addWishMovie(userId, movieId) {
            dispatch(addWishMovie(userId, movieId));
        },
        delWishMovie(userId, movieId) {
            dispatch(deleteWishMovie(userId, movieId));
        }
    };
}

export default connect(mapStateToProps, mapActionsToProps)(DetailsForMovieItem);    