import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

import { ApiResultModel } from '../models/api-result/api-result.model';
import { Movie } from '../models/movie.model';
import { MovieDetails } from '../models/movie-details';
import { ResultAction } from '../models/result-action';

@Injectable()
export class MovieService {
  moviesSubject = new BehaviorSubject<Array<Movie>>(new Array<Movie>());

  constructor(private http: HttpClient) { }

  fillPopularPlayingNowMovies(language: string) {
    return this.http
      .get<ApiResultModel>('movie/GetPopularPlayingNowMovies',
      {
        params: new HttpParams().set('language', language)
      })
      .pipe(map(apiResult => this.moviesSubject.next(this.getMovies(apiResult))))
      .subscribe(data => data, error => console.log(error));
  }

  private getMovies(apiResult: ApiResultModel) {
    return apiResult.results
      .map(m => new Movie(m.id, m.title, m.poster_path, m.vote_average, m.release_date));
  }

  getMovieDetails(movieId: number, language: string) {
    return this.http
      .get<MovieDetails>('movie/GetMovieByMovieId',
      {
        params: new HttpParams()
          .set('movieId', movieId.toString())
          .set('language', language)
      });
  }

  pushMovieForWatch(userId: string, movieId: number) {
    this.http.post<ResultAction>('movie/PushWishMovie',
      { user_id: userId, movie_id: movieId.toString() })
      .subscribe(data => data, error => console.log(error));
  }

  pullMovieForWatch(userId: string, movieId: number) {
    this.http.post<ResultAction>('movie/DeleteWishMovie',
      { user_id: userId, movie_id: movieId.toString() })
      .subscribe(data => data, error => console.log(error));
  }

  fillMoviesByGenreId(genreId: number, language: string) {
    return this.http
      .get<ApiResultModel>('movie/GetMoviesByGenreId',
      {
        params: new HttpParams()
          .set('genreId', genreId.toString())
          .set('language', language)
      })
      .pipe(map(apiResult => this.moviesSubject.next(this.getMovies(apiResult))))
      .subscribe(data => data, error => console.log(error));
  }
}
