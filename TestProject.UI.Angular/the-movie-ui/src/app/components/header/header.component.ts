import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  get currentUser() {
    return this.userService.currentUser;
  }

  get isUserGuest() {
    return this.currentUser.user_id == null;
  }

  constructor(private userService: UserService,
    private router: Router) { }

  logout() {
    this.userService.logout();
    this.router.navigate(['./']);
  }
}
