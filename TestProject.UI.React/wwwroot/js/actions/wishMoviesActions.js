﻿import axios from 'axios';
import React from 'react';

import { setWishMovies, addNewWishMovies, deleteWishMovies } from '../reducers/wishMoviesReducer';
import { WebConfig } from '../webConfig';

export const getWishMovies = (userId) => (dispatch, getState) => {
    axios.get(WebConfig.GET_WISH_MOVIES, { params: { userId } })
         .then(resp => {
            dispatch(setWishMovies(resp.data.map(wishMovie => wishMovie.movieId)));
         });
}

export const addWishMovie = (userId, movieId) => (dispatch, getState) => {
    axios.post(WebConfig.SET_WISH_MOVIE, { userId, movieId })
         .then(resp => {
            if (resp.data.isSuccess)
                dispatch(addNewWishMovies(movieId));
         });
}

export const deleteWishMovie = (userId, movieId) => (dispatch, getState) => {
    axios.post(WebConfig.DELETE_WISH_MOVIE, { userId, movieId })
         .then(resp => {
            if (resp.data.isSuccess)
                dispatch(deleteWishMovies(movieId));
         });
}
