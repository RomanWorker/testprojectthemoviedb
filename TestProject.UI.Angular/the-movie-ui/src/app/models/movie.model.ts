import { environment } from '../../environments/environment';

export class Movie {
    id: number;
    name: string;
    title: string;
    overview: string;
    posterPath: string;
    voteAverage: number;
    releaseDate: Date;

    get srcImgForPoster(): string {
        return environment.srcImg + this.posterPath;
    }

    constructor(id: number, title: string, posterPath: string,
        voteAverage: number, releaseDate: Date) {
        this.id = id;
        this.title = title;
        this.posterPath = posterPath;
        this.voteAverage = voteAverage;
        this.releaseDate = releaseDate;
    }
}
