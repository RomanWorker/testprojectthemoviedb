﻿using System.Collections.Generic;
using System.Linq;
using TestProject.API.Infrastructure.Entities;
using TestProject.API.Infrastructure.Interfaces;

namespace TestProject.API.Infrastructure.Concrete
{
    public class DbService : IDbService
    {
        private DbRemoteServer _dbRemoteServer;
        private DbLocalServer _dbLocalServer;
        private DbMongoServer _dbMongoServer;

        public DbService(string apiKey, string apiUrl, string connectionString, string mongoDbConnection, string mongoDbName)
        {
            _dbRemoteServer = new DbRemoteServer(apiKey, apiUrl);
            _dbLocalServer = new DbLocalServer(connectionString);
            _dbMongoServer = new DbMongoServer(mongoDbConnection, mongoDbName);
        }

        public string GetNowPlaying(int pageNumber = 1, string language = "en")
        {
            return _dbRemoteServer.GetNowPlaying(pageNumber, language).Json;
        }

        public string GetGenreForMovies(string language = "en")
        {
            return _dbRemoteServer.GetGenreForMovies(language).Json;
        }

        public List<UserCategory> GetUserCategories(int userid)
        {
            return _dbLocalServer.GetUserCategories(userid).ToList();
        }

        public string GetMoviesByGenreId(int genreId, int pageNumber = 1, string language = "en")
        {
            return _dbRemoteServer.GetMoviesByGenreId(genreId, pageNumber, language).Json;
        }

        public User GetAuthUser(string login, string passHash)
        {
            return _dbLocalServer.GetUser(null, login, passHash).FirstOrDefault();
        }

        public List<UserCategory> GetUserSettingCategories(int userId)
        {
            return _dbLocalServer.GetUserSettingCategories(userId).ToList();
        }

        public int UpdateUserToCategory(List<UserCategory> userCategories)
        {
            return _dbLocalServer.UpdateUserToCategory(userCategories);
        }

        public string GetMovieByMovieId(int movieId, string language = "en")
        {
            return _dbRemoteServer.GetMovieByMovieId(movieId, language).Json;
        }

        public string GetCreditsByMovieId(int movieId, string language = "en")
        {
            return _dbRemoteServer.GetCreditsByMovieId(movieId, language).Json;
        }

        public List<WishMovie> GetWishMovies(int userId)
        {
            return _dbLocalServer.GetWishMovies(userId).ToList();
        }

        public int SetWishMovie(WishMovie wishMovie)
        {
            return _dbLocalServer.SetWishMovie(wishMovie);
        }

        public int DeleteWishMovie(WishMovie wishMovie)
        {
            return _dbLocalServer.DeleteWishMovie(wishMovie);
        }

        public List<CustomMovie> GetWishListMovies(int userId, string language = "en")
        {
            var result = new List<CustomMovie>();
            var wishList = _dbLocalServer.GetWishMovies(userId).ToList();
            if (wishList.Count == 0)
                return result;

            wishList.ForEach(wishMovie => result.Add(
                new CustomMovie(_dbRemoteServer.GetMovieByMovieId(wishMovie.MovieId, language).Item)));
            return result;
        }

        public void AddNewUser(UserMongo newUser)
        {
            _dbMongoServer.InsertNewUser(newUser);
        }

        public UserMongo UserLogin(string userName, string password)
        {
            var foundUser = _dbMongoServer.GetUser(userName, password);
            return foundUser;
        }

        public void PushMovieForWatch(string userId, int movieId)
        {
            _dbMongoServer.PushMovieForWatch(userId, movieId);
        }

        public void PullMovieForWatch(string userId, int movieId)
        {
            _dbMongoServer.PullMovieForWatch(userId, movieId);
        }
    }
}
