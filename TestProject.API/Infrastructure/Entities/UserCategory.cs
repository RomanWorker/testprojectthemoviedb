﻿namespace TestProject.API.Infrastructure.Entities
{
    public class UserCategory
    {
        public int? UserId { get; set; }
        public int? CategoryId { get; set; }
        public int? OrderNumber { get; set; }
        public bool? IsShow { get; set; }
        public string CodeName { get; set; }
        public int? GenreId { get; set; }
    }
}
