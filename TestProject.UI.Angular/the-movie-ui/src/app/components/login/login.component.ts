import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';

import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  myFirstReactiveForm: FormGroup;
  errorMessage: string;

  constructor(private fb: FormBuilder,
    private userService: UserService,
    private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.myFirstReactiveForm = this.fb.group({
      username: ['', [ Validators.required ] ],
      password: ['', [ Validators.required ] ]
    });
   }

  isControlInvalid(control: AbstractControl): boolean {
    const result = control.invalid && control.touched;
    return result;
  }

  onSubmit() {
    const controls = this.myFirstReactiveForm.controls;
    if (this.myFirstReactiveForm.invalid) {
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }

    const user = this.getUser();
    this.userService.login(user)
        .subscribe(
          data => {
            if (data.isSuccess) {
              this.userService.currentUser = data.user;
              this.router.navigate(['./']);
            } else {
              this.errorMessage = data.errorMessage;
              console.log(data.errorMessage);
            }
          },
          error => console.dir(error));
  }

  private getUser() {
    return new User(
      this.myFirstReactiveForm.value.username,
      'en',
      null,
      null,
      null,
      this.myFirstReactiveForm.value.password,
    );
  }
}
