﻿import { createActions, handleActions } from 'redux-actions'

export const { setCategoryForMovies } = createActions({
    'SET_CATEGORY_FOR_MOVIES': categories => ({ categories })
});

export const categoryReducer = handleActions({
    [setCategoryForMovies]: (state, action) => action.payload.categories
}, []);
