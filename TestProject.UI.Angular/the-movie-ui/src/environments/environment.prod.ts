import { defaultConfig } from './environment.default';

export const environment = {
  ...defaultConfig,
  production: true
};
