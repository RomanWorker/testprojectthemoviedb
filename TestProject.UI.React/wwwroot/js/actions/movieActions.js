﻿import axios from 'axios';
import React from 'react';

import { setRespMovie } from '../reducers/movieReducer';
import { showSortMovies } from '../actions/showMoviesActions';
import { WebConfig } from '../webConfig';

export const getMoviesByGenreId = (params) => (dispatch, getState) => {
    getMoviesFromWebAPI(WebConfig.GET_MOVIES_BY_GENRE_ID,
        {
            genreId: params.genreId,
            pageNumber: params.pageNumber,
            language: params.language
        }, dispatch);
}

export const getPopularMoviesNow = (params) => (dispatch, getState) => {
    getMoviesFromWebAPI(WebConfig.GET_POPULAR_MOVIES_NOW_API,
        {
            pageNumber: params.pageNumber,
            language: params.language
        }, dispatch);
}

export const getWishListMovies = (params) => (dispatch, getState) => {
    getMoviesFromWebAPI(WebConfig.GET_WISH_MOVIES_FOR_USER,
        {
            userId: params.userId,
            language: params.language
        }, dispatch);
}

function getMoviesFromWebAPI(url, params, dispatch) {
    axios.get(url, { params })
         .then(resp => {
            dispatch(setRespMovie(resp.data));
            dispatch(showSortMovies(resp.data.results));
         });
}
