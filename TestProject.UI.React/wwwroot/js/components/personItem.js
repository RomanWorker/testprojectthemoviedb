﻿import React from 'react';

import { WebConfig } from '../webConfig';

export default class PersonItem extends React.Component {
    render() {
        const srcImg = this.props.srcImg;
        const personName = this.props.personName;
        const personJob = this.props.personJob;

        if (srcImg === null || personName.length === 0 || personJob.length === 0)
            return null;

        return (
            <div className="col-md-4 padding-left-0px margin-bottom-5px">
                <div className="col-md-4 padding-left-0px">
                    <img src={WebConfig.srcForImg + srcImg} alt="img" height="80" width="60" />
                </div>
                <div className="col-md-8 padding-left-0px">{personName}</div>
                <div className="col-md-8 padding-left-0px color-gray">({personJob})</div>
            </div>
        );
    }
}
