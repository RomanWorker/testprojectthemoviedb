﻿import { createActions, handleActions } from 'redux-actions'

import { enLanguage } from '../contents/en_local';
import { ruLanguage } from '../contents/ru_local';

export const { changeLocal } = createActions({
    'CHANGE_LOCAL': language => ({ language })
});

export const contentReducer = handleActions({
    [changeLocal]: (state, action) => getLocal(action.payload.language)
}, enLanguage);

function getLocal(local) {
    switch (local) {
        case 'en':
            return enLanguage;
        case 'ru':
            return ruLanguage;
        default:
            return enLanguage;
    }
}
