﻿using TestProject.API.Infrastructure.Interfaces;

namespace TestProject.API.Infrastructure.Concrete
{
    public class WebSettings : IWebSettings
    {
        public string KeyTMDApi { get; set; }
        public string UrlTMDApi { get; set; }
        public string ConnectionString { get; set; }
        public string MongoDbConnection { get; set; }
        public string MongoDbName { get; set; } 
    }
}
