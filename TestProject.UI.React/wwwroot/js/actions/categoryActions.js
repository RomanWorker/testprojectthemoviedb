﻿import axios from 'axios';
import React from 'react';

import { setCategoryForMovies } from '../reducers/categoryReducer';
import { setSelectedCategoryForMovies } from '../reducers/selectedCategoryReducer';

import { getPopularMoviesNow, getWishListMovies, getMoviesByGenreId } from '../actions/movieActions';
import { getWishMovies } from '../actions/wishMoviesActions';

import { WebConfig } from '../webConfig';

export const getCategoryForMovies = (userId) => (dispatch, getState) => {
    axios.get(WebConfig.GET_CATEGORIES_FOR_MOVIES_API, { params: { userId } })
        .then(resp => {
            let categories = mappingDispFuncShowMovies(resp.data);

            dispatch(setCategoryForMovies(categories));

            let selectedCategory = categories.find(category => category.codeName === 'popular');

            dispatch(setSelectedCategoryForMovies(selectedCategory));

            dispatch(selectedCategory.dispFuncGetMovies({
                pageNumber: 1,
                language: getState().content.language
            }));

            dispatch(getWishMovies(userId))
        });
}

function mappingDispFuncShowMovies(categories) {
    for (var i = 0; i < categories.length; i++) {
        if (categories[i].genreId > 0) {
            categories[i].dispFuncGetMovies = getMoviesByGenreId;
            continue;
        }

        if (categories[i].codeName === 'popular') {
            categories[i].dispFuncGetMovies = getPopularMoviesNow;
            continue;
        }

        if (categories[i].codeName === 'wishList') {
            categories[i].dispFuncGetMovies = getWishListMovies;
            continue;
        }

        categories[i].dispFuncGetMovies = getPopularMoviesNow;
    }
    return Array.from(categories);
}
