﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

using TestProject.API.Infrastructure.Concrete;
using TestProject.API.Infrastructure.Interfaces;

namespace TestProject.API.Controllers
{
    public abstract class BaseController : Controller
    {
        private IDbService _dbService;

        public IDbService DbService
        {
            get { return _dbService; }
        }

        public BaseController(IOptions<WebSettings> webSettings)
        {
            _dbService = new DbService(
                webSettings.Value.KeyTMDApi,
                webSettings.Value.UrlTMDApi,
                webSettings.Value.ConnectionString,
                webSettings.Value.MongoDbConnection,
                webSettings.Value.MongoDbName);
        }
    }
}
