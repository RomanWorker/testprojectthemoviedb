﻿import { createAction, handleActions } from 'redux-actions'

export const setShowMovies = createAction('SET_SHOW_MOVIES', movies => ({ movies }));

export const showMoviesReducer = handleActions({
    [setShowMovies]: (state, action) => action.payload.movies
}, []);
