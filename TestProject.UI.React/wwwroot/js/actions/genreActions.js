﻿import axios from 'axios';
import React from 'react';

import { setGenresForFilm } from '../reducers/genreReducer';
import { WebConfig } from '../webConfig';

export const getGenresForMovies = (language) => (dispatch, getState) => {
    axios.get(WebConfig.GET_GENRES_FOR_MOVIES_API, {
        params: { language }
    }).then(resp => { dispatch(setGenresForFilm(resp.data.genres)) });
}