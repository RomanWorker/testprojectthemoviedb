import { Component } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private translateService: TranslateService,
    private userService: UserService) {
    translateService.setDefaultLang('en');
  }
}
