﻿import React from 'react';
import { connect } from 'react-redux';

import { editShowCategory, doOrderUp, doOrderDown } from '../actions/userSettingsActions';

class SettingCategoryItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isShow: this.props.category.isShow,
            categoryId: this.props.category.categoryId
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.onOrderUpClick = this.onOrderUpClick.bind(this);
        this.onOrderDownClick = this.onOrderDownClick.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;

        this.setState({
            isShow: value
        });

        this.props.editShowCategory(this.state.categoryId, value);
    }

    getTitle(category) {
        return this.props.content.categoryMovies[category.codeName] || category.codeName;
    }

    onOrderUpClick(categoryId) {
        this.props.onOrderUp(categoryId);
    }

    onOrderDownClick(categoryId) {
        this.props.onOrderDown(categoryId);
    }

    render() {
        const category = this.props.category;
        const title = this.getTitle(category);
        let isDisabledBtn = !category.isShow;

        return (
            <div className="col-md-12 margin-bottom-5px">
                <div className="col-md-1">
                    <span className="margin-right-10px">#{category.orderNumber}</span>
                    <input
                        type="checkbox"
                        checked={this.state.isShow}
                        onChange={this.handleInputChange} />
                </div>
                <div className="col-md-1">
                    <div>
                        <button
                            className="btn btn-info"
                            disabled={isDisabledBtn}
                            onClick={() => this.onOrderUpClick(category.categoryId)}>/\</button>
                    </div>
                    <div>
                        <button
                            className="btn btn-info"
                            disabled={isDisabledBtn}
                            onClick={() => this.onOrderDownClick(category.categoryId)}>\/</button>
                    </div>
                </div>
                <div className="col-md-10">{title}</div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        content: state.content
    };
}

function mapActionsToProps(dispatch) {
    return {
        editShowCategory(categoryId, isShow) {
            dispatch(editShowCategory(categoryId, isShow));
        },
        onOrderUp(categoryId) {
            dispatch(doOrderUp(categoryId));
        },
        onOrderDown(categoryId) {
            dispatch(doOrderDown(categoryId));
        }
    };
}

export default connect(mapStateToProps, mapActionsToProps)(SettingCategoryItem);  
