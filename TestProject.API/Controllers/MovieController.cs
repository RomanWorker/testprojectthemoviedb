using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

using TestProject.API.Infrastructure.Concrete;
using TestProject.API.Infrastructure.Entities;

namespace TestProject.API.Controllers
{
    [Route("api/movie")]
    public class MovieController : BaseController
    {
        public MovieController(IOptions<WebSettings> webSettings) 
            : base(webSettings)  { }

        [HttpGet("GetPopularPlayingNowMovies")]
        public IActionResult GetPopularPlayingNowMovies(int pageNumber = 1, string language = "en")
        {
            return Ok(DbService.GetNowPlaying(pageNumber, language));
        }

        [HttpGet("GetMoviesByGenreId")]
        public IActionResult GetMoviesByGenreId(int genreId, int pageNumber = 1, string language = "en")
        {
            return Ok(DbService.GetMoviesByGenreId(genreId, pageNumber, language));
        }

        [HttpGet("GetMovieByMovieId")]
        public IActionResult GetMovieByMovieId(int movieId, string language = "en")
        {
            return Ok(DbService.GetMovieByMovieId(movieId, language));
        }

        [HttpGet("GetCreditsByMovieId")]
        public IActionResult GetCreditsByMovieId(int movieId, string language = "en")
        {
            return Ok(DbService.GetCreditsByMovieId(movieId, language));
        }

        [HttpGet("GetWishMovies")]
        public IActionResult GetWishMovies(int userId)
        {
            return Ok(DbService.GetWishMovies(userId));
        }

        [HttpPost("PushWishMovie")]
        public IActionResult SetWishMovie([FromBody] WishMovie wishMovie)
        {
            DbService.PushMovieForWatch(wishMovie.UserId, wishMovie.MovieId);
            return Ok(new { isSuccess = true });
        }

        [HttpPost("DeleteWishMovie")]
        public IActionResult DeleteWishMovie([FromBody] WishMovie wishMovie)
        {
            DbService.PullMovieForWatch(wishMovie.UserId, wishMovie.MovieId);
            return Ok(new { isSuccess = true });
        }

        [HttpGet("GetWishMoviesForUser")]
        public IActionResult GetWishMoviesForUser(int userId, string language = "en")
        {
            return Ok(new { results = DbService.GetWishListMovies(userId, language) });
        }
    }
}