﻿import axios from 'axios';
import React from 'react';

import { setCreditsForMovies } from '../reducers/creditsMoviesReducer';
import { WebConfig } from '../webConfig';

const COUNT_SHOW_ACTORS = 5;

export const getCreditsByMovieId = (movieId, language) => (dispatch, getState) => {
    axios.get(WebConfig.GET_CREDITS_BY_MOVIE_ID, {
        params: { movieId, language }
    }).then(resp => {
        let creditsMovie = resp.data;
        creditsMovie.countShowActors = COUNT_SHOW_ACTORS;
        dispatch(setCreditsForMovies(creditsMovie));
    });
}

export const setShowActors = (movieId, countShowActors) => (dispatch, getState) => {
    let creditsMovie = getState().creditsMovies.find(credits => credits.id === movieId);
    creditsMovie.countShowActors = countShowActors;
    dispatch(setCreditsForMovies(creditsMovie));
}
