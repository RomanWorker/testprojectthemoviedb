export class User {
    first_name: string;
    last_name: string;
    user_name: string;
    email: string;
    password: string;
    language: string;
    movies_for_watch: Array<number>;
    user_id: string;

    constructor(userName: string, language: string,
        firstName?: string, lastName?: string, email?: string,
        password?: string, moviesForWatch?:  Array<number>, userId?: string)  {
        this.first_name = firstName;
        this.last_name = lastName;
        this.user_name = userName;
        this.email = email;
        this.password = password;
        this.language = language;
        this.movies_for_watch = moviesForWatch;
        this.user_id = userId;
    }
}
