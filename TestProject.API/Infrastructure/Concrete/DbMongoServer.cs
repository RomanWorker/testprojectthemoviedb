﻿using MongoDB.Bson;
using MongoDB.Driver;

using System;
using System.Linq;

using TestProject.API.Infrastructure.Entities;

namespace TestProject.API.Infrastructure.Concrete
{
    public class DbMongoServer
    {
        private readonly string _mongoDbConnectionString;
        private readonly string _mongoDbName;

        private IMongoCollection<UserMongo> _userMongoCollection;

        private IMongoCollection<UserMongo> UserMongoCollection
        {
            get
            {
                return _userMongoCollection ?? (_userMongoCollection
                    = GetCollection<UserMongo>("Users"));
            }
        }

        private IMongoCollection<T> GetCollection<T>(string collectionName)
        {
            var client = new MongoClient(_mongoDbConnectionString);
            IMongoDatabase database = client.GetDatabase(_mongoDbName);
            return database.GetCollection<T>(collectionName);
        }

        public DbMongoServer(string mongoDbConnection, string mongoDbName)
        {
            _mongoDbConnectionString = mongoDbConnection;
            _mongoDbName = mongoDbName;
        }

        public void InsertNewUser(UserMongo newUser)
        {
            UserMongoCollection.InsertOne(newUser);
        }

        public UserMongo GetUser(string userName, string password)
        {
            return UserMongoCollection
                .Find(u => u.UserName == userName && u.Pass == password)
                .FirstOrDefault();
        }

        public void PushMovieForWatch(string userId, int movieId)
        {
            var tmpUser = UserMongoCollection
                .Find(u => u.Id == ObjectId.Parse(userId))
                .FirstOrDefault();
            if (tmpUser.MoviesForWatch != null && tmpUser.MoviesForWatch.Contains(movieId))
            {
                return;
            }
            UserMongoCollection.UpdateOne(
                u => u.Id == ObjectId.Parse(userId),
                Builders<UserMongo>.Update
                    .Push(pm => pm.MoviesForWatch, movieId)
                    .Set(pm => pm.UpdateOn, DateTime.UtcNow));
        }

        public void PullMovieForWatch(string userId, int movieId)
        {
            UserMongoCollection.UpdateOne(
                u => u.Id == ObjectId.Parse(userId),
                Builders<UserMongo>.Update
                    .Pull(pm => pm.MoviesForWatch, movieId)
                    .Set(pm => pm.UpdateOn, DateTime.UtcNow));
        }
    }
}
