using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using TestProject.API.Infrastructure.Concrete;
using TestProject.API.Infrastructure.Entities;

namespace TestProject.API.Controllers
{
    [Route("api/categorie")]
    public class CategorieController : BaseController
    {
        public CategorieController(IOptions<WebSettings> webSettings) 
            : base(webSettings) { }

        [HttpGet("GetUserCategories")]
        public IActionResult GetUserCategories(int userId)
        {
            var respons = DbService.GetUserCategories(userId);
            return Ok(respons);
        }

        [HttpGet("GetUserSettingCategories")]
        public IActionResult GetUserSettingCategories(int userId)
        {
            var respons = DbService.GetUserSettingCategories(userId);
            return Ok(respons.OrderBy(category => category.OrderNumber));
        }

        [HttpPost("PostUserSettingCategories")]
        public IActionResult PostUserSettingCategories([FromBody] List<UserCategory> categories)
        {
            try
            {
                DbService.UpdateUserToCategory(categories);
                return Ok(new { isSuccess = true });
            }
            catch(Exception ex)
            {
                return Ok(new { isSuccess = false, errorMessage = ex.Message });
            }
            
        }
    }
}