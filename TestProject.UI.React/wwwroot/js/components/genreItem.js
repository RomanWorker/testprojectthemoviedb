﻿import React from 'react';

export default class GenreItem extends React.Component {
    constructor(props) {
        super(props);
    }

    getStrGenres(allGenres, itemGenres) {
        let tmpResult = '';

        if (allGenres.length === 0)
            return tmpResult;

        for (var i = 0; i < itemGenres.length; i++) {
            if (i > 0)
                tmpResult += ', ';

            tmpResult += allGenres.find((genre) => {
                return genre.id === itemGenres[i];
            }).name;
        }

        return tmpResult;
    }

    render() {
        const allGenres = this.props.allGenres;
        const itemGenres = this.props.genres;

        return (
            <div className="color-gray">
                {this.getStrGenres(allGenres, itemGenres)}
            </div>
        );
    }
}
