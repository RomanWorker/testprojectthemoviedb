﻿import axios from 'axios';
import React from 'react';

import { setUser } from '../reducers/userReducer';
import { getCategoryForMovies } from '../actions/categoryActions';
import { WebConfig } from '../webConfig';

export const tryAuthUser = (user) => (dispatch, getState) => {
    axios.get(WebConfig.AUTH_API, {
        params: { login: user.login, password: user.password }
    }).then(resp => {
        let user = resp.data;

        dispatch(setUser(user))

        if (user.error === null) {
            dispatch(getCategoryForMovies(user.userId));
        }
    });
}
