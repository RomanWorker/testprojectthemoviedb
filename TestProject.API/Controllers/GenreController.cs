using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using TestProject.API.Infrastructure.Concrete;

namespace TestProject.API.Controllers
{
    [Route("api/genre")]
    public class GenreController : BaseController
    {
        public GenreController(IOptions<WebSettings> webSettings) 
            : base(webSettings) { }

        [HttpGet("GetForMovies")]
        public IActionResult GetForMovies(string language = "en")
        {
            return Ok(DbService.GetGenreForMovies(language));
        }
    }
}