import { ApiMovieModule } from './api-movie.model';

export class ApiResultModel {
  results: ApiMovieModule[];
}
