﻿import { createAction, handleActions } from 'redux-actions'

export const setRespMovie = createAction('SET_RESP_MOVIE', respMovie => ({ respMovie }));

export const movieReducer = handleActions({
    [setRespMovie]: (state, action) => action.payload.respMovie
}, { results: [] });
