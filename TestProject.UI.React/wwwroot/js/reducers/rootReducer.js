﻿import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import { userReducer } from './userReducer';
import { contentReducer } from './contentReducer';
import { movieReducer } from './movieReducer';
import { genreReducer } from './genreReducer';
import { categoryReducer } from './categoryReducer';
import { showMoviesReducer } from './showMoviesReducer';
import { sortReducer } from './sortReducer';
import { selectedCategoryReducer } from './selectedCategoryReducer';
import { userSettingsReducer } from './userSettingsReducer';
import { detailsMoviesReducer } from './detailsMoviesReducer';
import { creditsMoviesReducer } from './creditsMoviesReducer';
import { wishMoviesReducer } from './wishMoviesReducer';

export const RootReducer = combineReducers({
    routing: routerReducer,
    user: userReducer,
    content: contentReducer,
    responseMovie: movieReducer,
    showMovies: showMoviesReducer,
    sortMovies: sortReducer,
    detailsMovies: detailsMoviesReducer,
    genresForMovies: genreReducer,
    categoryMovies: categoryReducer,
    selectedCategory: selectedCategoryReducer,
    userSettings: userSettingsReducer,
    creditsMovies: creditsMoviesReducer,
    wishMovies: wishMoviesReducer
});
