﻿using Newtonsoft.Json;

namespace TestProject.API.Infrastructure.Entities
{
    public class WishMovie
    {
        [JsonProperty(PropertyName = "user_id")]
        public string UserId { get; set; }

        [JsonProperty(PropertyName = "movie_id")]
        public int MovieId { get; set; }
    }
}
