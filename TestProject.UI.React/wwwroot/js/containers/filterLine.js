﻿import React from 'react';
import { connect } from 'react-redux';

import { showFilterMoviesByTitle } from '../actions/showMoviesActions';

class FilterLine extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            filterLine: ''
        }

        this.onChange = this.onChange.bind(this);
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
        this.doFilter(e.target.value);
    }

    doFilter(filterLine) {
        if (filterLine.length > 1)
            this.props.filterAction(filterLine);
        else 
            this.props.filterAction(null);
    }

    render() {
        return (
            <div>
                <input
                    value={this.state.filterLine}
                    onChange={this.onChange}
                    type="text"
                    name="filterLine"
                    className="form-control width-50per"
                    placeholder={this.props.content.filterPlaceholder}
                />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        content: state.content
    };
}

function mapActionsToProps(dispatch) {
    return {
        filterAction(filterStr) {
            dispatch(showFilterMoviesByTitle(filterStr));
        }
    };
}

export default connect(mapStateToProps, mapActionsToProps)(FilterLine);    