﻿const baseUrl = 'http://localhost:53267/api/'

export const WebConfig = {
    srcForImg: 'http://image.tmdb.org/t/p/w154',
    AUTH_API: baseUrl + 'auth/GetAuthUser',
    GET_GENRES_FOR_MOVIES_API: baseUrl + 'genre/GetForMovies',
    GET_POPULAR_MOVIES_NOW_API: baseUrl + 'movie/GetPopularPlayingNowMovies',
    IMAGE_PATH: '/images/',
    srcLogoForLeftMenu: '/images/powered-by-rectangle-blue.png',
    GET_CATEGORIES_FOR_MOVIES_API: baseUrl + 'categorie/GetUserCategories',
    GET_MOVIES_BY_GENRE_ID: baseUrl + 'movie/GetMoviesByGenreId',
    GET_USER_SETTING_CATEGORIES_API: baseUrl + 'categorie/GetUserSettingCategories',
    POST_USER_SETTING_CATEGORIES_API: baseUrl + 'categorie/PostUserSettingCategories',
    GET_MOVIE_BY_MOVIE_ID: baseUrl + 'movie/GetMovieByMovieId',
    GET_CREDITS_BY_MOVIE_ID: baseUrl + 'movie/GetCreditsByMovieId',
    GET_WISH_MOVIES: baseUrl + 'movie/GetWishMovies',
    SET_WISH_MOVIE: baseUrl + 'movie/SetWishMovie',
    DELETE_WISH_MOVIE: baseUrl + 'movie/DeleteWishMovie',
    GET_WISH_MOVIES_FOR_USER: baseUrl + 'movie/GetWishMoviesForUser'
}