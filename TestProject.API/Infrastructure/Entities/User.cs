﻿namespace TestProject.API.Infrastructure.Entities
{
    public class User
    {
        public int UserId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool IsAuth { get; set; }
        public string Error { get; set; }
    }
}
