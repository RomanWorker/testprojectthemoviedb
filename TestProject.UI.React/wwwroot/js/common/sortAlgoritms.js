﻿export const SortByRatingUp = {
    codeName: 'SortByRatingUp',
    func(a, b) {
        return b.vote_average - a.vote_average;
    }
}

export const SortByRatingDown = {
    codeName: 'SortByRatingDown',
    func(a, b) {
        return a.vote_average - b.vote_average;
    }
}

export const SortByYearUp = {
    codeName: 'SortByYearUp',
    func(a, b) {
        return new Date(b.release_date) - new Date(a.release_date);
    }
}

export const SortByYearDown = {
    codeName: 'SortByYearDown',
    func(a, b) {
        return new Date(a.release_date) - new Date(b.release_date);
    }
}
