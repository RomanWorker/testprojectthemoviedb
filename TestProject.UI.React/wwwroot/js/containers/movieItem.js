﻿import React from 'react';
import { connect } from 'react-redux';

import GenreItem from '../components/genreItem';
import StarsVoteItem from '../components/starsVoteItem';
import DetailsForMovieItem from './detailsForMoviesItem';

import { getDetailsForMovie } from '../actions/detailsMoviesActions';
import { getCreditsByMovieId } from '../actions/creditsActions';

import { WebConfig } from '../webConfig';

class MovieItem extends React.Component {
    constructor(props) {
        super(props);
    }

    viewDetails(movieId, isShowPage) {
        this.props.getDetailsForMovie(movieId, this.props.content.language, isShowPage);
    }

    getBtnForViewDetails(movieId, details) {
        let isShowPage = details !== undefined ? details.isShowPage : false;

        if (isShowPage) {
            return <button
                className="btn btn-info"
                onClick={() => { this.viewDetails(movieId, false) }}>/\</button>
        } else {
            return <button
                className="btn btn-info"
                onClick={() => { this.viewDetails(movieId, true) }}>\/</button>
        }
    }

    render() {
        const movie = this.props.movie;
        const srcImg = WebConfig.srcForImg + movie.poster_path;
        const releaseYear = movie.release_date.substring(0, 4);
        const voteAverage = this.props.movie.vote_average;
        const voteCount = this.props.movie.vote_count;
        const details = this.props.detailsMovies.find(dtls => dtls.id === movie.id);
        const btnForViewDetails = this.getBtnForViewDetails(movie.id, details);

        return (
            <div className="row margin-bottom-5px">
                <div className="col-md-1">
                    <img src={srcImg} alt="img" height="100" width="60" />
                </div>
                <div className="col-md-11">
                    <div className="col-md-6">
                        <div>{movie.title} ({releaseYear})</div>
                        <GenreItem
                            genres={movie.genre_ids}
                            allGenres={this.props.allGenres}
                            content={this.props.content}
                        />
                    </div>
                    <div className="col-md-4">
                        <StarsVoteItem
                            voteAverage={voteAverage}
                            voteCount={voteCount}
                            content={this.props.content}
                        />
                    </div>
                    <div className="col-md-1">
                        {btnForViewDetails}
                    </div>
                    <div className="col-md-12">
                        <DetailsForMovieItem movieId={movie.id} />
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        detailsMovies: state.detailsMovies,
        allGenres: state.genresForMovies,
        content: state.content
    };
}

function mapActionsToProps(dispatch) {
    return {
        getDetailsForMovie(movieId, language, isShowPage) {
            dispatch(getDetailsForMovie(movieId, language, isShowPage));
            dispatch(getCreditsByMovieId(movieId, language));
        }
    };
}

export default connect(mapStateToProps, mapActionsToProps)(MovieItem);    