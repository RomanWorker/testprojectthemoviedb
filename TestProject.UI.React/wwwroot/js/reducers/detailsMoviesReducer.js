﻿import { createAction, handleActions } from 'redux-actions'

export const setDetailsForMovies = createAction('SET_DETAILS_FOR_MOVIE', detailsMovie => ({ detailsMovie }));

export const detailsMoviesReducer = handleActions({
    [setDetailsForMovies]: (state, action) => addNewDetails(action.payload.detailsMovie, state)
}, []);

function addNewDetails(detailsMovie, details) {
    let tmpIndex = details.findIndex(detail => detail.id === detailsMovie.id);

    if (tmpIndex > -1) {
        details[tmpIndex] = detailsMovie;
    } else {
        details.push(detailsMovie);
    }

    return Array.from(details);
}
