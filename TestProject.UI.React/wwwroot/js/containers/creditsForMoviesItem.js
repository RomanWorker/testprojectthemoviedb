﻿import React from 'react';
import { connect } from 'react-redux';

import PersonItem from '../components/personItem';
import { setShowActors } from '../actions/creditsActions';

import { WebConfig } from '../webConfig';

class CreditsForMovieItem extends React.Component {
    constructor(props) {
        super(props);

        this.showMoreActors = this.showMoreActors.bind(this);
    }

    showMoreActors(movieId, countShowActors) {
        this.props.setCountShowCountActors(movieId, countShowActors);
    }

    getActors(credits) {
        let actors = []
        let countActors = 0;

        for (var i = 0; i < credits.cast.length; i++) {
            let actor = credits.cast[i];

            if (actor.profile_path === null || actor.name.length === 0 || actor.character.length === 0)
                continue;

            actors.push(
                <PersonItem
                    key={actor.id}
                    srcImg={actor.profile_path}
                    personName={actor.name}
                    personJob={actor.character}
                />
            );

            countActors++;

            if (countActors === credits.countShowActors)
                break;
        }
    
        return actors;
    }

    getBtnForActors(credits, btnName) {
        if (credits.countShowActors === credits.cast.length)
            return null;

        return (
            <div className="col-md-4 padding-left-0px margin-top-45px">
                <button
                    className="btn btn-info"
                    onClick={() => this.showMoreActors(credits.id, credits.cast.length)}>
                    {btnName}
                </button>
            </div>
        );
    }

    render() {
        const credits = this.props.creditsMovies.find(credit => credit.id === this.props.movieId);

        if (credits == undefined)
            return null;

        const content = this.props.content;
        const director = credits.crew.find(crw => crw.job === 'Director');
        const actors = this.getActors(credits);
        const btnMoreForActors = this.getBtnForActors(credits, content.btnMore);

        return (
            <div>
                <div className="col-md-12 padding-left-0px">
                    <PersonItem
                        srcImg={director.profile_path}
                        personName={director.name}
                        personJob={content.director}
                    />
                </div>
                {actors}
                {btnMoreForActors}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        creditsMovies: state.creditsMovies,
        content: state.content
    };
}

function mapActionsToProps(dispatch) {
    return {
        setCountShowCountActors(movieId, countShowActors) {
            dispatch(setShowActors(movieId, countShowActors));
        }
    };
}

export default connect(mapStateToProps, mapActionsToProps)(CreditsForMovieItem);    