﻿import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { changeLocal } from '../reducers/contentReducer';
import { getGenresForMovies } from '../actions/genreActions';
import { tryAuthUser } from '../actions/authActions';
import { updateLanguageDetails } from '../actions/detailsMoviesActions';

class NavigationBar extends React.Component {
    onChangeLocal(language, event) {
        event.preventDefault();

        this.props.changeLanguage(this.props.selectedCategory, {
            genreId: this.props.selectedCategory.genreId,
            userId: this.props.user.userId,
            pageNumber: 1,
            language: language
        });
    }

    onLogout() {
        event.preventDefault();
        this.props.doLogout();
    }

    render() {
        const content = this.props.content;
        const user = this.props.user;
        const authButton = user.isAuth === true
            ? <a href="#" onClick={this.onLogout.bind(this)}>{content.btnLogout}</a>
            : <Link to="/login">{content.btnLogin}</Link>
        const btnSettings = user.isAuth === true
            ? <Link to="/settings" className="navbar-brand">{content.btnSettings}</Link>
            : ''

        return (
            <nav className="navbar navbar-default">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <Link to="/" className="navbar-brand">{content.home}</Link>
                        <span className="navbar-brand">{user.login}</span>
                        {btnSettings}
                    </div>
                    <div className="collapse navbar-collapse">
                        <ul className="nav navbar-nav navbar-right">
                            <li><a href="#" onClick={this.onChangeLocal.bind(this, 'ru')}>RU</a></li>
                            <li><a href="#" onClick={this.onChangeLocal.bind(this, 'en')}>EN</a></li>
                            <li>{authButton}</li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.user,
        selectedCategory: state.selectedCategory,
        content: state.content
    };
}

function mapActionsToProps(dispatch) {
    return {
        changeLanguage(selectedCotegory, params) {
            dispatch(changeLocal(params.language));
            dispatch(getGenresForMovies(params.language));
            dispatch(selectedCotegory.dispFuncGetMovies(params));
            dispatch(updateLanguageDetails(params.language));
        },
        doLogout() {
            dispatch(tryAuthUser({ login: 'guest', password: 'guest' }));
        }
    }
}

export default connect(mapStateToProps, mapActionsToProps)(NavigationBar);