﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;

using TestProject.API.Infrastructure.Concrete;
using TestProject.API.Infrastructure.Entities;
using TestProject.API.Models;

namespace TestProject.API.Controllers
{
    [Route("api/auth")]
    public class AuthController : BaseController
    {
        public AuthController(IOptions<WebSettings> webSettings) 
            : base(webSettings) { }

        [HttpGet("GetAuthUser")]
        public IActionResult GetAuthUser(string login, string password)
        {
            var authUser = DbService.GetAuthUser(login, password);

            if (authUser == null)
            {
                return BadRequest();
            }

            authUser.Password = null;
            authUser.IsAuth = !authUser.Login.Equals("guest");

            return Ok(authUser);
        }

        [HttpPost("AuthUser")]
        public IActionResult AddNewUser([FromBody]UserModel user)
        {
            try
            {
                if (string.IsNullOrEmpty(user.FirstName) || string.IsNullOrEmpty(user.LastName) 
                    || string.IsNullOrEmpty(user.UserName) || string.IsNullOrEmpty(user.Email)
                    || string.IsNullOrEmpty(user.Pass))
                {
                    return BadRequest("Validation failed");
                }
                var newUser = GetNewUser(user);
                DbService.AddNewUser(newUser);
                return Ok(new { isSuccess = true });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("Login")]
        public IActionResult Login([FromBody]UserModel user)
        {
            try
            {
                var newUser = GetNewUser(user);
                var foundUser = DbService.UserLogin(user.UserName, user.Pass);
                return foundUser != null
                    ? Ok(new { isSuccess = true, user = GetUserInfo(foundUser) }) 
                    : Ok(new
                    {
                        isSuccess = false,
                        errorMessage = "wrong login or password!"
                    });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private UserInfoModel GetUserInfo(UserMongo user)
        {
            return new UserInfoModel()
            {
                UserId = user.Id.ToString(),
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserName = user.UserName,
                Email = user.Email,
                MoviesForWatch = user.MoviesForWatch
            };
        }

        private UserMongo GetNewUser(UserModel user)
        {
            return new UserMongo()
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserName = user.UserName,
                Email = user.Email,
                Pass = user.Pass,
                MoviesForWatch = new List<int>(),
                CreateOn = DateTime.UtcNow,
                UpdateOn = DateTime.UtcNow
            };
        }
    }
}
