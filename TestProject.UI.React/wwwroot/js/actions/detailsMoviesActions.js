﻿import axios from 'axios';
import React from 'react';

import { setDetailsForMovies } from '../reducers/detailsMoviesReducer';
import { WebConfig } from '../webConfig';

export const getDetailsForMovie = (movieId, language, isShowPage) => (dispatch, getState) => {
    let detailsMovies = getState().detailsMovies;
    let detail = detailsMovies.find(detail => detail.id === movieId);

    if (detail !== undefined) {
        let tmpDetail = Object.assign({}, detail);
        tmpDetail.isShowPage = isShowPage;
        dispatch(setDetailsForMovies(tmpDetail));
    } else {
        useDetailFromDM(dispatch, { movieId, language, isShowPage })
    }
}

export const updateLanguageDetails = (language) => (dispatch, getState) => {
    let detailsMovies = getState().detailsMovies;

    detailsMovies.forEach(details => {
        useDetailFromDM(dispatch,
            {
                movieId: details.id,
                language,
                isShowPage: details.isShowPage
            });
    });
}

function useDetailFromDM(dispatch, params) {
    axios.get(WebConfig.GET_MOVIE_BY_MOVIE_ID, {
        params: {
            movieId: params.movieId,
            language: params.language
        }
    }).then(resp => {
        let tmpDetail = Object.assign({}, resp.data);
        tmpDetail.isShowPage = params.isShowPage;
        dispatch(setDetailsForMovies(tmpDetail));
    });
}
