import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import {Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppComponent } from './app.component';
import { MovieComponent } from './components/movie/movie.component';
import { MoviesComponent } from './components/movies/movies.component';
import { VoteComponent } from './components/vote/vote.component';
import { MovieService } from './services/movie.service';
import { UserService } from './services/user.service';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { LanguageSwitcherComponent } from './components/language-switcher/language-switcher.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ApiInterceptor } from './interceptors/ApiInterceptor';
import { MovieDetailsComponent } from './components/movie-details/movie-details.component';
import { MoviesPageComponent } from './components/movies-page/movies-page.component';
import { HeaderComponent } from './components/header/header.component';
import { MoviesGenreComponent } from './components/movies-genre/movies-genre.component';

export function translateHttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

const appRoutes: Routes = [
  { path: '', component: MoviesPageComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    MovieComponent,
    MoviesComponent,
    VoteComponent,
    NotFoundComponent,
    LanguageSwitcherComponent,
    LoginComponent,
    RegisterComponent,
    MovieDetailsComponent,
    MoviesPageComponent,
    HeaderComponent,
    MoviesGenreComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: translateHttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    MovieService,
    UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
