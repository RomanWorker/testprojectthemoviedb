﻿import React from 'react';

export default class CategoryItem extends React.Component {
    static get selectedCategoryClass() {
        return 'btn btn-group-justified';
    }
    static get simpleCategoryClass() {
        return 'btn btn-info btn-group-justified';
    }

    constructor(props) {
        super(props);

        this.onClick = this.onClick.bind(this);
    }

    onClick(category) {
        this.props.clickAction(category);
    }

    getTitle(category) {
        return this.props.content.categoryMovies[category.codeName] || category.codeName;
    }

    getClass(category, selectedCategory) {
        if (selectedCategory === null)
            return CategoryItem.simpleCategoryClass;

        return category.categoryId === selectedCategory.categoryId
            ? CategoryItem.selectedCategoryClass
            : CategoryItem.simpleCategoryClass;
    }

    render() {
        const category = this.props.category;
        const selectedCategory = this.props.selectedCategory;

        const title = this.getTitle(category);
        const className = this.getClass(category, selectedCategory);

        return (
            <div>
                <button
                    onClick={() => this.onClick(category)}
                    className={className}>
                    {title}
                </button>
            </div>
        );
    }
}
