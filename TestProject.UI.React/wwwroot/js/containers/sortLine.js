﻿import React from 'react';
import { connect } from 'react-redux';

import { setSortForMovies } from '../reducers/sortReducer';
import { showFilterMoviesByTitle } from '../actions/showMoviesActions';
import { SortByRatingUp, SortByRatingDown, SortByYearUp, SortByYearDown } from '../common/sortAlgoritms';

class SortLine extends React.Component {
    static get selectedSortClass() {
        return 'btn width-150px margin-right-10px';
    }
    static get simpleSortClass() {
        return 'btn btn-info width-150px margin-right-10px';
    }

    constructor(props) {
        super(props);
        
        this.onClick = this.onClick.bind(this);
    }

    onClick(func) {
        this.props.changeSortFunc(func);
    }

    render() {
        const content = this.props.content;
        const sortMovies = this.props.sortMovies;

        return (
            <div>
                <button
                    className={sortMovies.codeName === 'SortByRatingUp' ? SortLine.selectedSortClass : SortLine.simpleSortClass}
                    name="SortByRatingUp"
                    onClick={() => this.onClick(SortByRatingUp)}>
                    {content.byRatingUp}
                </button>
                <button
                    className={sortMovies.codeName === 'SortByRatingDown' ? SortLine.selectedSortClass : SortLine.simpleSortClass}
                    name="SortByRatingDown"
                    onClick={() => this.onClick(SortByRatingDown)}>
                    {content.byRatingDown}
                </button>
                <button
                    className={sortMovies.codeName === 'SortByYearUp' ? SortLine.selectedSortClass : SortLine.simpleSortClass}
                    name="SortByYearUp"
                    onClick={() => this.onClick(SortByYearUp)}>
                    {content.byYearUp}
                </button>
                <button
                    className={sortMovies.codeName === 'SortByYearDown' ? SortLine.selectedSortClass : SortLine.simpleSortClass}
                    name="SortByYearDown"
                    onClick={this.onClick}
                    onClick={() => this.onClick(SortByYearDown)}>
                    {content.byYearDown}
                </button>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        sortMovies: state.sortMovies,
        content: state.content
    };
}

function mapActionsToProps(dispatch) {
    return {
        changeSortFunc(sortFunc) {
            dispatch(setSortForMovies(sortFunc));
            dispatch(showFilterMoviesByTitle());
        }
    };
}

export default connect(mapStateToProps, mapActionsToProps)(SortLine);    