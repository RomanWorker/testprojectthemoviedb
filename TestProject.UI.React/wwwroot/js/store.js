﻿import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import { RootReducer } from './reducers/rootReducer';

function logger({ getState }) {
    return function (next) {
        return function (action) {
            console.log('will dispatch', action);
            const state = next(action);
            console.log('state after dispatch', getState());
            return state;
        }
    }
}

const store = createStore(
    RootReducer,
    applyMiddleware(logger, thunk)
);

export default store;