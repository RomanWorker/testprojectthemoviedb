﻿using System.Collections.Generic;
using TestProject.API.Infrastructure.Entities;

namespace TestProject.API.Infrastructure.Interfaces
{
    public interface IDbService
    {
        string GetNowPlaying(int pageNumber = 1, string language = "en");
        string GetGenreForMovies(string language = "en");
        string GetMoviesByGenreId(int genreId, int pageNumber = 1, string language = "en");
        List<UserCategory> GetUserCategories(int userid);
        List<UserCategory> GetUserSettingCategories(int userId);
        User GetAuthUser(string login, string passHash);
        int UpdateUserToCategory(List<UserCategory> userCategories);
        string GetMovieByMovieId(int movieId, string language = "en");
        string GetCreditsByMovieId(int movieId, string language = "en");
        List<WishMovie> GetWishMovies(int userId);
        int SetWishMovie(WishMovie wishMovie);
        int DeleteWishMovie(WishMovie wishMovie);
        List<CustomMovie> GetWishListMovies(int userId, string language = "en");

        void AddNewUser(UserMongo newUser);
        UserMongo UserLogin(string userName, string password);
        void PushMovieForWatch(string userId, int movieId);
        void PullMovieForWatch(string userId, int movieId);
    }
}
