﻿import React from 'react';
import { connect } from 'react-redux';

import { WebConfig } from '../webConfig';
import CategoryItem from '../components/categoryItem';

import { setSelectedCategoryForMovies } from '../reducers/selectedCategoryReducer';

const RECTANGLE_IMG = 'powered-by-rectangle-blue.png';

class LeftMenu extends React.Component {
    constructor(props) {
        super(props);

        this.clickAction = this.clickAction.bind(this);
    }

    clickAction(category) {
        this.props.changeSelectedCategory(category);
        this.props.changeCategory(category, {
            genreId: category.genreId,
            pageNumber: 1,
            language: this.props.content.language,
            userId: this.props.user.userId
        });
    }

    render() {
        const sortCategories = this.props.categoryMovies.sort(
            (a, b) => {
                return a.orderNumber > b.orderNumber
            });

        return (
            <div>
                <div className="margin-bottom-20px">
                    <img src={WebConfig.IMAGE_PATH + RECTANGLE_IMG} width="160" />
                </div>
                <div>
                    {sortCategories.map(category =>
                        <CategoryItem
                            key={category.categoryId}
                            category={category}
                            selectedCategory={this.props.selectedCategory}
                            content={this.props.content}
                            clickAction={this.clickAction}
                        />
                    )}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.user,
        categoryMovies: state.categoryMovies,
        selectedCategory: state.selectedCategory,
        content: state.content
    };
}

function mapActionsToProps(dispatch) {
    return {
        changeCategory(category, params) {
            dispatch(category.dispFuncGetMovies(params));
        },
        changeSelectedCategory(category) {
            dispatch(setSelectedCategoryForMovies(category));
        }
    };
}

export default connect(mapStateToProps, mapActionsToProps)(LeftMenu);  