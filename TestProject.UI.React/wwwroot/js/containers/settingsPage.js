﻿import React from 'react';
import { connect } from 'react-redux';

import SettingCategoryItem from './settingCategoryItem';

import { getUserSettingsCategories, postUserSettingCategories } from '../actions/userSettingsActions';

class SettingsPage extends React.Component {
    constructor(props) {
        super(props);

        if (this.props.userSettings.categories.length < 1)
            this.props.getCategoriesForUser(this.props.user.userId);

        this.onClick = this.onClick.bind(this);
    }

    getSortSettingCategories(categories) {
        return categories.sort(
            (a, b) => {
                let tmpA = a.orderNumber !== null ? a.orderNumber : 999;
                let tmpB = b.orderNumber !== null ? b.orderNumber : 999;
                return tmpA > tmpB;
            });
    }

    onClick() {
        let userId = this.props.user.userId;
        let categories = this.props.userSettings.categories
            .map(category => {
                category.userId = userId;
                return category;
            })

        this.props.postCategoriesForUser(categories);
    }

    render() {
        const content = this.props.content;
        const userSettings = this.props.userSettings;
        const settingCategories = this.getSortSettingCategories(userSettings.categories);

        const actionMessage = getActionMessage(userSettings, content);

        return (
            <div>
                <div className="margin-bottom-10px">
                    <button
                        className="btn btn-info margin-right-10px"
                        onClick={this.onClick}>
                        {content.btnSaveSettings}
                    </button>
                    <span>{actionMessage}</span>
                </div>
                {settingCategories.map(category => (<SettingCategoryItem key={category.categoryId} category={category} />))}
            </div>
        );
    }
}

function getActionMessage(userSettings, content) {
    if (userSettings.isSuccess === null || userSettings.isSuccess === undefined)
        return '';

    return userSettings.isSuccess ? content.success : content.fail;
}

function mapStateToProps(state) {
    return {
        user: state.user,
        userSettings: state.userSettings,
        content: state.content
    };
}

function mapActionsToProps(dispatch) {
    return {
        getCategoriesForUser(userId) {
            dispatch(getUserSettingsCategories(userId));
        },
        postCategoriesForUser(categories) {
            dispatch(postUserSettingCategories(categories));
        }
    };
}

export default connect(mapStateToProps, mapActionsToProps)(SettingsPage);