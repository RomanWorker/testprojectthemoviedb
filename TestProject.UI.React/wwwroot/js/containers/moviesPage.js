﻿import React from 'react';
import { connect } from 'react-redux';

import LeftMenu from './leftMenu';
import MovieItem from './movieItem';
import FilterLine from './filterLine';
import SortLine from './sortLine';

import { getGenresForMovies } from '../actions/genreActions';
import { getCategoryForMovies } from '../actions/categoryActions';

class MoviesPage extends React.Component {
    constructor(props) {
        super(props);

        if (this.props.allGenres.length < 1)
            this.props.getGenresForFilms(this.props.content.local);

        if (this.props.categories.length < 1)
            this.props.getCategoriesForMovies(this.props.user.userId);
    }

    render() {
        const showMovies = this.props.showMovies;

        return (
            <div>
                <div className="col-md-2">
                    <LeftMenu />
                </div>
                <div className="col-md-8">
                    <div className="margin-bottom-5px">
                        <FilterLine />
                    </div>
                    <div className="margin-bottom-10px">
                        <SortLine />
                    </div>
                    <div>
                        {showMovies.map(movie => (<MovieItem key={movie.id} movie={movie} />))}
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.user,
        showMovies: state.showMovies,
        allGenres: state.genresForMovies,
        categories: state.categoryMovies,
        selectedCategory: state.selectedCategory,
        content: state.content
    };
}

function mapActionsToProps(dispatch) {
    return {
        getGenresForFilms(local) {
            dispatch(getGenresForMovies(local));
        },
        getCategoriesForMovies(userId) {
            dispatch(getCategoryForMovies(userId));
        }
    }
}

export default connect(mapStateToProps, mapActionsToProps)(MoviesPage);

