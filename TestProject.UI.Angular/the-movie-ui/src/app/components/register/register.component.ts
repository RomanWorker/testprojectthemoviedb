import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';

import { UserService } from '../../services/user.service';
import { User } from '../../models/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  reactiveForm: FormGroup;

  constructor(private fb: FormBuilder,
    private userService: UserService,
    private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.reactiveForm = this.fb.group({
      firstname: ['', [ Validators.required ] ],
      lastname: ['', [ Validators.required ] ],
      email: ['', [ Validators.required, Validators.email ] ],
      username: ['', [ Validators.required ] ],
      password: ['', [ Validators.required ] ]
    });
   }

  isControlInvalid(control: AbstractControl): boolean {
    const result = control.invalid && control.touched;
    return result;
  }

  onSubmit() {
    const controls = this.reactiveForm.controls;
    if (this.reactiveForm.invalid) {
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }
    const newUser = this.getNewUser();
    this.userService.registrate(newUser)
        .subscribe(
          data => {
            if (data.isSuccess) {
              this.router.navigate(['./login']);
            } else {
              console.log(data.errorMessage);
            }
          },
          error => console.dir(error));
  }

  private getNewUser() {
    return new User(
      this.reactiveForm.value.username,
      'en',
      this.reactiveForm.value.firstname,
      this.reactiveForm.value.lastname,
      this.reactiveForm.value.email,
      this.reactiveForm.value.password,
    );
  }
}
