﻿import axios from 'axios';
import React from 'react';

import { setShowMovies } from '../reducers/showMoviesReducer';

export const showFilterMoviesByTitle = (filterStr = null) => (dispatch, getState) => {
    let showMovies = getResiltFilterByTitle(filterStr, getState().responseMovie.results);

    showMovies = getSortMovies(getState().sortMovies.func, showMovies);

    dispatch(setShowMovies(showMovies));
}

export const showSortMovies = (movies) => (dispatch, getState) => {
    let showMovies = getSortMovies(getState().sortMovies.func, movies);
    dispatch(setShowMovies(showMovies));
}

function getResiltFilterByTitle(filterStr, movies) {
    if (filterStr === null)
        return movies;

    const filter = filterStr.toLowerCase();

    return movies.filter(movie => {
        if (movie.title.toLowerCase().indexOf(filter) !== -1)
            return movie;
    })
}

function getSortMovies(sortFunc, movies) {
    if (sortFunc === null)
        return movies;

    movies.sort(sortFunc);

    return Array.from(movies);
}