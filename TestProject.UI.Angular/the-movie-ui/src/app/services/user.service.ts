import { environment } from './../../environments/environment.prod';
import { ResultUserAction } from './../models/result-user-action';
import { ResultAction } from '../models/result-action';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, BehaviorSubject } from 'rxjs';

import { Md5 } from 'ts-md5';

import { TranslateService } from '@ngx-translate/core';

import { User } from '../models/user';

@Injectable()
export class UserService {
  private _currentUser: User;

  currentLangSubject = new BehaviorSubject<string>(environment.defLanguage);

  constructor(private http: HttpClient, private translateService: TranslateService) {
    this._currentUser = new User('Guest', environment.defLanguage);
    this.currentLangSubject.subscribe(
      lang => this._currentUser.language = lang);
  }

  get currentUser(): User {
    return this._currentUser;
  }

  set currentUser(newUser: User) {
    this._currentUser = newUser;
  }

  registrate(newUser: User) {
    newUser.password = Md5.hashStr(newUser.password).toString();
    return this.http.post<ResultAction>('auth/AuthUser', newUser);
  }

  login(newUser: User) {
    newUser.password = Md5.hashStr(newUser.password).toString();
    return this.http.post<ResultUserAction>('auth/Login', newUser);
  }

  logout() {
    this._currentUser = new User('Guest', environment.defLanguage);
    this.changeLanguage(environment.defLanguage);
  }

  changeLanguage(language) {
    this.currentLangSubject.next(language);
    this.translateService.use(language);
  }

  get currentLanguage(): Observable<string> {
    return this.currentLangSubject.asObservable();
  }
}
