﻿import React from 'react';
import ReactDOM from 'react-dom';

import { BrowserRouter, HashRouter, Router} from 'react-router-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';

import store from './store';
import Routes from './routes';

import createHistory from 'history/createBrowserHistory';

const history = createHistory();

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter  history={history}>
            <Routes />
        </ConnectedRouter >
    </Provider>
    , document.getElementById('content')
);
