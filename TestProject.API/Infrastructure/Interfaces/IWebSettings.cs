﻿namespace TestProject.API.Infrastructure.Interfaces
{
    public interface IWebSettings
    {
        string KeyTMDApi { get; set; }
        string UrlTMDApi { get; set; }
        string ConnectionString { get; set; }
        string MongoDbConnection { get; set; }
        string MongoDbName { get; set; }
    }
}
