﻿using DM.MovieApi.MovieDb.Movies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace TestProject.API.Infrastructure.Entities
{
    public class CustomMovie
    {
        [DataMember(Name = "vote_average")]
        public double vote_average { get; set; }
        [DataMember(Name = "video")]
        public bool IsVideo { get; set; }
        [DataMember(Name = "tagline")]
        public string Tagline { get; set; }
        [DataMember(Name = "status")]
        public string Status { get; set; }
        [DataMember(Name = "runtime")]
        public int Runtime { get; set; }
        [DataMember(Name = "revenue")]
        public decimal Revenue { get; set; }
        [DataMember(Name = "release_date")]
        public DateTime release_date { get; set; }
        [DataMember(Name = "poster_path")]
        public string poster_path { get; set; }
        [DataMember(Name = "popularity")]
        public double Popularity { get; set; }
        [DataMember(Name = "overview")]
        public string Overview { get; set; }
        [DataMember(Name = "genres")]
        public List<int> genre_ids { get; set; }
        [DataMember(Name = "budget")]
        public int Budget { get; set; }
        [DataMember(Name = "title")]
        public string Title { get; set; }
        [DataMember(Name = "id")]
        public int Id { get; set; }
        [DataMember(Name = "vote_count")]
        public int vote_count { get; set; }

        public CustomMovie(Movie movie)
        {
            Title = movie.Title;
            vote_average = movie.VoteAverage;
            release_date = movie.ReleaseDate;
            Overview = movie.Overview;
            Id = movie.Id;
            vote_average = movie.VoteAverage;
            vote_count = movie.VoteCount;
            Budget = movie.Budget;
            genre_ids = movie.Genres.Select(genre => genre.Id).ToList();
            poster_path = movie.PosterPath;
            Runtime = movie.Runtime;
        }
    }
}
