import { Component } from '@angular/core';

import { MovieService } from '../../services/movie.service';
import { UserService } from '../../services/user.service';

enum Genre {
  Popular = 0,
  Action = 28,
  Adventure = 12,
  Animation = 16,
  Comedy = 35
}

@Component({
  selector: 'app-movies-genre',
  templateUrl: './movies-genre.component.html',
  styleUrls: ['./movies-genre.component.css']
})
export class MoviesGenreComponent {
  currentGenre = Genre.Popular;
  genreType = Genre;

  constructor(private movieService: MovieService,
    private userService: UserService) {
      userService.currentLanguage
        .subscribe(lang => this.currentGenre = Genre.Popular);
  }

  clickGenreBtn(genre: Genre) {
    this.currentGenre = genre;
    const lang = this.userService.currentUser.language;
    if (genre === Genre.Popular) {
      this.movieService.fillPopularPlayingNowMovies(lang);
      return;
    }
    this.movieService.fillMoviesByGenreId(genre.valueOf(), lang);
  }
}
