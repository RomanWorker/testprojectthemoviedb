import { Component, OnInit, Input } from '@angular/core';

import { Vote } from '../../models/vote.model';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.css']
})
export class VoteComponent implements OnInit {
  @Input() voteNumber: number;
  votes: Array<Vote> = new Array(10).fill(new Vote(environment.srcBadVote));

  constructor() { }

  ngOnInit() {
    const goodVotes = Math.round(this.voteNumber);
    this.votes.fill(new Vote(environment.srcGoodVote), 0, goodVotes);
  }
}
