import { Component, Input } from '@angular/core';

import { MovieDetails } from '../../models/movie-details';
import { MovieService } from '../../services/movie.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent {
  @Input() movieId: number;
  movieDetails: MovieDetails;
  isShowDetails = false;

  constructor(private movieService: MovieService,
    private userService: UserService) { }

  clickShowDetails() {
    if (this.movieDetails != null) {
      this.isShowDetails = !this.isShowDetails;
      return;
    }
    this.movieService.getMovieDetails(
      this.movieId, this.userService.currentUser.language)
      .subscribe(result => {
        this.movieDetails = result;
        this.isShowDetails = !this.isShowDetails;
      });
  }
}
