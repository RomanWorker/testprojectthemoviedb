import { Component, Input } from '@angular/core';

import { Movie } from '../../models/movie.model';
import { MovieService } from '../../services/movie.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent {
  @Input() movie: Movie;

  get isNotGuest() {
    return this.userService.currentUser.user_id !== undefined;
  }

  get isForWatch() {
    const user = this.userService.currentUser;
    if (user.movies_for_watch === null) {
      return false;
    }
    const movieId = user.movies_for_watch
      .find(m => m === this.movie.id);
    return movieId !== undefined;
  }

  constructor(private movieService: MovieService,
    private userService: UserService) { }

  onChangeIsForWatch() {
    if (this.isForWatch) {
      this.pullMovieForWatch();
      return;
    }
    this.pushMovieForWatch();
  }

  private pullMovieForWatch() {
    const currentUser = this.userService.currentUser;
    this.movieService.pullMovieForWatch(
      currentUser.user_id,
      this.movie.id
    );
    currentUser.movies_for_watch = currentUser.movies_for_watch
      .filter(item => item !== this.movie.id);
  }

  private pushMovieForWatch() {
    const currentUser = this.userService.currentUser;
    this.movieService.pushMovieForWatch(
      currentUser.user_id,
      this.movie.id
    );
    currentUser.movies_for_watch.push(this.movie.id);
  }
}
