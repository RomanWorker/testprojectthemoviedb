﻿import { createAction, handleActions } from 'redux-actions'

export const setGenresForFilm = createAction('SET_GENRES_FOR_MOVIES', genres => ({ genres }));

export const genreReducer = handleActions({
    [setGenresForFilm]: (state, action) => action.payload.genres
}, []);
