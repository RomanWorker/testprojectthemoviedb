﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace TestProject.API.Infrastructure.Entities
{
    public class UserMongo
    {
        [BsonElement("_id")]
        public ObjectId Id { get; set; }

        [BsonElement("first_name")]
        public string FirstName { get; set; }

        [BsonElement("last_name")]
        public string LastName { get; set; }

        [BsonElement("user_name")]
        public string UserName { get; set; }

        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("password")]
        public string Pass { get; set; }

        [BsonElement("movies_for_watch")]
        public IEnumerable<int> MoviesForWatch { get; set; }

        [BsonElement("updateon")]
        public DateTime UpdateOn { get; set; }

        [BsonElement("createon")]
        public DateTime CreateOn { get; set; }
    }
}
