﻿import React from 'react';
import { Redirect } from 'react-router';

export default class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            login: '',
            password: ''
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    onSubmit(e) {
        e.preventDefault();
        this.props.loginAction(this.state);
    }

    render() {
        const user = this.props.user;
        const content = this.props.content;

        let errorMessage;
        if (user.error !== null && user.error.length > 0)
            errorMessage = <div className="alert alert-danger">{user.error}</div>;

        let goRedirect;
        if (user.isAuth)
            goRedirect = <Redirect to="/movies" />

        return (
            <form onSubmit={this.onSubmit}>
                {goRedirect}

                {errorMessage} 

                <div className="form-group">
                    <label className="control-label">{content.login}</label>
                    <input
                        value={this.state.login}
                        onChange={this.onChange}
                        type="text"
                        name="login"
                        className="form-control"
                    />
                </div>

                <div className="form-group">
                    <label className="control-label">{content.password}</label>
                    <input
                        value={this.state.password}
                        onChange={this.onChange}
                        type="password"
                        name="password"
                        className="form-control"
                    />
                </div>

                <div>
                    <button className="btn btn-primary btn-lg">
                        {content.btnLogin}
                    </button>
                </div>
            </form>
        );
    }
}
