import { Component } from '@angular/core';

import { Movie } from '../../models/movie.model';
import { MovieService } from '../../services/movie.service';
import { UserService } from '../../services/user.service';

enum MovieSort {
  RatingUp,
  RatingDown,
  YearUp,
  YearDown
}

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent {
  private _movies: Array<Movie>;
  private _filterTitle = '';
  private _currentSort = this.sortByRatingDown();

  currentMovieSort = MovieSort.RatingDown;
  movieSortType = MovieSort;

  get showMovies() {
    return  this._movies.filter(
      m => m.title.toLowerCase().startsWith(this._filterTitle));
  }

  constructor(private movieService: MovieService,
    private userService: UserService) {
      movieService.moviesSubject
        .subscribe(movies => this._movies = movies.sort(this._currentSort));
      userService.currentLanguage
        .subscribe(lang => movieService.fillPopularPlayingNowMovies(lang));
  }

  filterByTitle(filterTitle: string) {
    this._filterTitle = filterTitle;
  }

  clickSortBtn(sortFunc: (a, b) => number, sort: MovieSort) {
    this._currentSort = sortFunc;
    this.currentMovieSort = sort;
    this.doSortMovies();
  }

  doSortMovies() {
    this.movieService.moviesSubject.next(this._movies.sort(this._currentSort));
  }

  sortByRatingUp() {
    return (m1, m2) => m1.voteAverage - m2.voteAverage;
  }

  sortByRatingDown() {
    return (m1, m2) => m2.voteAverage - m1.voteAverage;
  }

  sortByYearUp() {
    return (m1, m2) =>  m1.releaseDate > m2.releaseDate ? 1 : -1;
  }

  sortByYearDown() {
    return (m1, m2) =>  m1.releaseDate > m2.releaseDate ? -1 : 1;
  }
}
