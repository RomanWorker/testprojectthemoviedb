﻿import { createAction, handleActions } from 'redux-actions'

import { SortByRatingUp } from '../common/sortAlgoritms';

const SET_SORT_FOR_MOVIES = 'SET_SORT_FOR_MOVIES';

export const setSortForMovies = createAction(SET_SORT_FOR_MOVIES, sortBy => ({ sortBy }));

export const sortReducer = handleActions({
    [setSortForMovies]: (state, action) => action.payload.sortBy
}, SortByRatingUp);
