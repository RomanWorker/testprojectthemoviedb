﻿import axios from 'axios';
import React from 'react';

import { setUserSettings } from '../reducers/userSettingsReducer';
import { getCategoryForMovies } from '../actions/categoryActions';
import { WebConfig } from '../webConfig';

const DO_ORDER_UP = 'DO_ORDER_UP';
const DO_ORDER_DOWN = 'DO_ORDER_DOWN';

export const getUserSettingsCategories = (userId) => (dispatch, getState) => {
    axios.get(WebConfig.GET_USER_SETTING_CATEGORIES_API, {
        params: { userId }
    }).then(resp => {
        dispatch(setUserSettings({ categories: resp.data }))
    });
}

export const postUserSettingCategories = (categories) => (dispatch, getState) => {
    axios.post(WebConfig.POST_USER_SETTING_CATEGORIES_API, categories)
        .then(resp => {
            let isSuccess = resp.data.isSuccess;
            let newSettings = Object.assign({}, getState().userSettings);
            newSettings.isSuccess = isSuccess;
            dispatch(setUserSettings(newSettings));

            if (isSuccess)
                dispatch(getCategoryForMovies(getState().user.userId));
        });
}

export const editShowCategory = (categoryId, isShow) => (dispatch, getState) => {
    let userSettings = getState().userSettings;

    let newCategories = getCategoriesAfterEditShow(userSettings.categories, categoryId, isShow);

    let sortCategories = getSortCategories(newCategories);

    let newUserSettings = Object.assign({}, userSettings);
    newUserSettings.categories = sortCategories;

    dispatch(setUserSettings(newUserSettings));
}

function getCategoriesAfterEditShow(categories, categoryId, isShow) {
    return categories.map(category => {
        if (category.categoryId === categoryId) {
            category.isShow = isShow;
            category.orderNumber = isShow ? 888 : null;
        }
        return category;
    })
}

function getSortCategories(categories) {
    let sortCategories = categories.sort(
        (a, b) => {
            let tmpA = a.orderNumber !== null ? a.orderNumber : 999;
            let tmpB = b.orderNumber !== null ? b.orderNumber : 999;
            return tmpA > tmpB;
        }
    );

    for (var i = 0; i < sortCategories.length; i++) {
        if (sortCategories[i].isShow === true) {
            sortCategories[i] = Object.assign({}, sortCategories[i]);
            sortCategories[i].orderNumber = i + 1;
        }
    }

    return sortCategories;
}

export const doOrderUp = (categoryId) => (dispatch, getState) => {
    changeOrderCategories(categoryId, DO_ORDER_UP, dispatch, getState);
}

export const doOrderDown = (categoryId) => (dispatch, getState) => {
    changeOrderCategories(categoryId, DO_ORDER_DOWN, dispatch, getState);
}

function changeOrderCategories(categoryId, type, dispatch, getState) {
    let categories = getState().userSettings.categories;

    switch (type) {
        case DO_ORDER_UP:
            categories = orderUpCategories(categories, categoryId);
            break;
        case DO_ORDER_DOWN:
            categories = orderDownCategories(categories, categoryId);
            break;
    }

    if (categories === null)
        return;

    let newUserSettings = Object.assign({}, getState().userSettings);
    newUserSettings.categories = categories;

    dispatch(setUserSettings(newUserSettings));
}

function orderUpCategories(categories, categoryId) {
    for (var i = 0; i < categories.length; i++) {
        if (categories[i].categoryId !== categoryId)
            continue;

        if (i < 1)
            return null;

        let tmp1 = Object.assign({}, categories[i]);
        tmp1.orderNumber = tmp1.orderNumber - 1;

        let tmp2 = Object.assign({}, categories[i - 1]);
        tmp2.orderNumber = tmp2.orderNumber + 1;

        categories[i - 1] = tmp1;
        categories[i] = tmp2;
        break;
    }
    return categories;
}

function orderDownCategories(categories, categoryId) {
    for (var i = 0; i < categories.length; i++) {
        if (categories[i].categoryId !== categoryId)
            continue;

        if (i === categories.length || categories[i + 1].orderNumber === null)
            return null;

        let tmp1 = Object.assign({}, categories[i]);
        tmp1.orderNumber = tmp1.orderNumber + 1;

        let tmp2 = Object.assign({}, categories[i + 1]);
        tmp2.orderNumber = tmp2.orderNumber - 1;

        categories[i + 1] = tmp1;
        categories[i] = tmp2;
        break;
    }
    return categories;
}
